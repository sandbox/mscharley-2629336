<?php

namespace Drupal\heroku\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class HerokuDatabaseForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'heroku_database_confirm_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $databases = heroku_database_settings();

    if (getenv('DYNO') && $databases['default']['default']['driver'] == 'sqlite') {
      drupal_set_message(t('SQLite can not be persisted on Heroku. This is fine for testing but means that when your dyno resets (when you deploy code or every 24 hours) you will lose your data.'), 'warning');
    }

    $form['#title'] = t('Available databases');

    $form['databases'] = [
      '#type' => 'table',
      '#header' => [t('Provider'), t('Driver'), t('Host'), t('Database')],
    ];

    // #weight doesn't work in install profile, so we reorder the data
    // manually.
    $default = $databases['default'];
    unset($databases['default']);
    $databases = array_merge(['default' => $default], $databases);

    foreach ($databases as $provider => $connection) {
      $form['databases'][$provider] = [
        'provider' => ['#plain_text' => $provider],
        'driver' => ['#plain_text' => $connection['default']['driver']],
        'host' => ['#plain_text' => @$connection['default']['host']],
        'database' => ['#plain_text' => $connection['default']['database']],
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Confirm'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $build_info['args'][0]['parameters']['heroku_db'] = 'ok';
    $form_state->setBuildInfo($build_info);
  }
}

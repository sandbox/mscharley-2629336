<?php

namespace Drupal\heroku\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class S3ConfigurationForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'heroku_install_s3';
  }

  /**
   * Form constructor.
   *
   * Most of this function was borrowed from \Drupal\s3fs\Form\SettingsForm.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = t('S3 setup');
    $form['explanation']['#plain_text'] = t('Heroku doesn\'t support storing files directly on their servers. You need a third-party host for uploaded content such as S3. If you don\'t have your own details your own account then you can get a free bucket via the Bucketeer addon.');

    $region_map = array(
      '' => 'Default',
      'us-east-1' => 'US Standard (us-east-1)',
      'us-west-1' => 'US West - Northern California  (us-west-1)',
      'us-west-2' => 'US West - Oregon (us-west-2)',
      'us-gov-west-1' => 'USA GovCloud Standard (us-gov-west-1)',
      'eu-west-1' => 'EU - Ireland  (eu-west-1)',
      'eu-central-1' => 'EU - Frankfurt (eu-central-1)',
      'ap-southeast-1' => 'Asia Pacific - Singapore (ap-southeast-1)',
      'ap-southeast-2' => 'Asia Pacific - Sydney (ap-southeast-2)',
      'ap-northeast-1' => 'Asia Pacific - Tokyo (ap-northeast-1)',
      'sa-east-1' => 'South America - Sao Paulo (sa-east-1)',
      'cn-north-1' => 'China - Beijing (cn-north-1)',
    );

    $form['credentials'] = array(
      '#type' => 'fieldset',
      '#title' => t('Amazon Web Services Credentials'),
      '#description' => t(
        "To configure your Amazon Web Services credentials, enter the values in the appropriate fields below.
        You may instead set \$conf['awssdk2_access_key'] and \$conf['awssdk2_secret_key'] in your site's settings.php   file.
        Values set in settings.php will override the values in these fields."
      ),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['credentials']['access_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon Web Services Access Key'),
      '#default_value' => getenv('BUCKETEER_AWS_ACCESS_KEY_ID'),
      '#size' => 44,
      '#required' => TRUE,
    );

    $form['credentials']['secret_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon Web Services Secret Key'),
      '#default_value' =>  getenv('BUCKETEER_AWS_SECRET_ACCESS_KEY'),
      '#size' => 44,
      '#required' => TRUE,
    );

    $form['bucket'] = array(
      '#type'           => 'textfield',
      '#title'          => t('S3 Bucket Name'),
      '#default_value'  => getenv('BUCKETEER_BUCKET_NAME'),
      '#required'       => TRUE,
    );
    $form['region'] = array(
      '#type'          => 'select',
      '#options'       => $region_map,
      '#title'         => t('S3 Region'),
      '#default_value' => '',
      '#description'   => t(
        'The region in which your bucket resides. Be careful to specify this accurately,
      as you are likely to see strange or broken behavior if the region is set wrong.<br>
      Use of the USA GovCloud region requires @SPECIAL_PERMISSION.<br>
      Use of the China - Beijing region requires a @CHINESE_AWS_ACCT.',
        array(
          '@CHINESE_AWS_ACCT' => \Drupal::l('亚马逊 AWS account', Url::fromUri('http://www.amazonaws.cn')),
          '@SPECIAL_PERMISSION' => \Drupal::l('special permission', Url::fromUri('http://aws.amazon.com/govcloud-us/')),
        )
      ),
    );

    $form['root_folder'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Root Folder'),
      '#default_value'  => getenv('BUCKETEER_BUCKET_PREFIX'),
      '#description'    => t(
        'S3 File System uses the specified folder as the root of the file system within your bucket (if blank, the bucket
      root is used). This is helpful when your bucket is used by multiple sites, or has additional data in it which
      s3fs should not interfere with. <br>
      Bucketeer uses this by default if you are on a shared plan. You can add extra subfolders beneath their default.<br>
      The metadata refresh function will not retrieve metadata for any files which are outside the Root Folder.<br>
      This setting is case sensitive. Do not include leading or trailing slashes.'
      ),
    );

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Continue'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory()->getEditable('s3fs.settings')
      ->set('access_key', $values['access_key'])
      ->set('secret_key', $values['secret_key'])
      ->set('bucket', $values['bucket'])
      ->set('region', $values['region'])
      ->set('use_https', 1)
      ->set('use_s3_for_public', 1)
      ->set('use_s3_for_private', 1)
      ->set('root_folder', trim($values['root_folder'], '\/'))
      ->save();
  }
}

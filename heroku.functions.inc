<?php
/**
 * @file
 * Functions required for managing Heroku settings.
 *
 * This file is require_once'd but heroku.settings.inc must be included via
 * require as it may be sourced multiple times.
 *
 * @see heroku.settings.inc
 */

const HEROKU_DEFAULT_SYNC_DIRECTORY = 'sites/default/files/config_X6eQXd1Zzt7yt25QLHoAd24BXi5NkrBG6KXZ0UlmQVggyGnXYinfKnhUa4Okssaev8c2Yxj2Lg/sync';
const HEROKU_DEFAULT_HASH_SALT = '76SIHGVWpwdT0U6dP8omkSAN5YbbtOBPHc2rG26xqYv9H-rV8OMirpPuncF-iGtIWMfDtdUaAg';

/**
 * Generates an array of database connection details from environment settings.
 *
 * This is compatible with $databases and it's recommended to simply assign
 * the output of the function to that variable.
 *
 * @return array
 */
function heroku_database_settings() {
  $database_variables = [
    // MySQL options. Recommended option as many contrib modules support MySQL much better than Postgres.
    'JAWSDB_URL' => 'jawsdb',
    'CLEARDB_DATABASE_URL' => 'cleardb',
    // Heroku Postgres.
    'DATABASE_URL' => 'heroku-postgresql',
  ];
  $scheme_mapping = [
    'postgres' => 'pgsql',
    'mysql2' => 'mysql',
  ];
  $settings = [];
  $dbconn = NULL;

  foreach ($database_variables as $var => $name) {
    $dbconn = getenv($var);

    if ($dbconn) {
      $db_url = parse_url($dbconn);

      $conn['driver'] = empty($scheme_mapping[$db_url['scheme']]) ? $db_url['scheme'] : $scheme_mapping[$db_url['scheme']];
      $conn['database'] = substr($db_url['path'], 1);
      $conn['username'] = $db_url['user'];
      $conn['password'] = $db_url['pass'];
      $conn['host'] = $db_url['host'];
      if (!empty($db_url['port'])) {
        $conn['port'] = $db_url['port'];
      }

      $settings[$name]['default'] = $conn;
      if (empty($settings['default'])) {
        $settings['default']['default'] = $conn;
      }
    }
  }

  $sqlite_db = DRUPAL_ROOT . '/sites/default/files/.ht.sqlite';
  $database_exists = file_exists($sqlite_db);
  if ($database_exists || empty($settings)) {
    $settings['sqlite']['default'] = [
      'driver' => 'sqlite',
      'database' => $sqlite_db,
    ];

    if (!$database_exists) {
      $db = new PDO('sqlite:' . $sqlite_db);
    }

    if (empty($settings['default'])) {
      $settings['default'] = $settings['sqlite'];
    }
  }

  return $settings;
}

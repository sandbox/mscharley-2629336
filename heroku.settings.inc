<?php
/**
 * @file
 * Settings file to include in your settings.php.
 *
 * This file should be included using the require function *not* require_once
 * as settings.php can and will be sourced multiple times and these settings
 * need to be pushed through every time.
 *
 * @see heroku.functions.inc
 */

require_once __DIR__ . '/heroku.functions.inc';

$databases = heroku_database_settings();

$settings['install_profile'] = 'heroku';

$config_directories['sync'] = getenv('DRUPAL_SYNC_DIRECTORY');
if (empty($config_directories['sync'])) {
  $config_directories['sync'] = HEROKU_DEFAULT_SYNC_DIRECTORY;
}

$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT');
if (empty($settings['hash_salt'])) {
  $settings['hash_salt'] = HEROKU_DEFAULT_HASH_SALT;
}
